
import logging

import ffmpy
import time
import os
import websocket
import json
import ssl
import boto3
#logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', level=logging.DEBUG)
#  ffmpeg -i Game.of.Thrones.S07E07.1080p.mkv -vcodec mpeg4 -b 4000k -acodec mp2 -ab 320k converted.avi


class VideoConversion(object):
    def __init__(self, _config_):
        #Session AWS
        self._config_=_config_
        self.session = boto3.Session(
            aws_access_key_id= _config_.get_aws_accesskey(),
            aws_secret_access_key= _config_.get_aws_secretkey(),
            region_name= _config_.get_dynamodb_region()
        )
        self.awsDynamoDB = self.session.resource("dynamodb")
        self.awsS3 = self.session.resource("s3")
        self.awsS3Client = boto3.client("s3")
        #self.video_conversion_collection = self.db[_config_.get_video_conversion_collection()]
        #self.url = _config_.get_video_status_callback_url()
        self.bucket = self.awsS3.Bucket(_config_.get_aws_bucket())
        self.table = self.awsDynamoDB.Table(_config_.get_dynamodb_table())

    def convert(self, _id_, _uri_):
        # Download du fichier
        self.bucket.download_file(_uri_,"C:/Users/Lazar/AppData/Local/Temp/GotS7E7_intro.mkv")
        converted = _uri_.replace(".mkv", "-converted.avi")
        logging.info('Converting...'+_id_+_uri_)
        converted_file = converted
        os.rename(_uri_,converted)
        # Upload du fichier vers le bucket amazon
        self.awsS3Client.upload_file(converted_file,self._config_.get_aws_bucket(),converted_file)
        # Suppresion du fichier apres upload
        os.remove(converted_file)
        # Mise à jour de la table pour faire remonter l'information
        self.table.update_item(
                Key={'uuid':_id_},
                UpdateExpression='SET converted = :cle', ExpressionAttributeValues={
                    ":cle": 1}
                )
        #print (os.getcwd())
        #ff = ffmpy.FFmpeg(
        #        inputs={_uri_: None},
        #        outputs={converted : '-y -vcodec mpeg4 -b 4000k -acodec mp2 -ab 320k' }
        #    )
        #logging.info("FFMPEG = %s", ff.cmd)
        #ff.run()
        #
        #payload = dict()
        #payload["id"] = _id_;
        #payload["status"] = 0;
        #
        #json_payload = json.dumps(payload)
        #logging.info("payload = %s", json_payload)
