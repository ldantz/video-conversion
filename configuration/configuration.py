import yaml
import logging

#logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', level=logging.DEBUG)


class Configuration(object):
    def __init__(self):
        self.configuration_file = "application.yml" # Euuuuuurk !
        self.configuration_data = None
        f = open(self.configuration_file, 'r')
        self.configuration_data = yaml.load(f.read())
        f.close()

    def get_dynamodb_endpoint(self):
        return self.configuration_data['amazon']['dynamodb']['endpoint']

    def get_dynamodb_region(self):
        return self.configuration_data['amazon']['aws']['region']

    def get_dynamodb_table(self):
        return self.configuration_data['amazon']['dynamodb']['table']

    def get_aws_accesskey(self):
        return self.configuration_data['amazon']['aws']['accesskey']

    def get_aws_secretkey(self):
        return self.configuration_data['amazon']['aws']['secretkey']

    def get_aws_bucket(self):
        return self.configuration_data['amazon']['aws']['bucket']

    def get_pubsub_project(self):
        return self.configuration_data['google']['project']

    def get_pubsub_topic(self):
        return self.configuration_data['google']['topic']

    def get_pubsub_subscription(self):
        return self.configuration_data['google']['subscription']