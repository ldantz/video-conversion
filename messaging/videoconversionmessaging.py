
from threading import Thread
import logging
import json
import queue
import time
from google.cloud import pubsub_v1

#logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', level=logging.DEBUG)

class VideoConversionMessaging(Thread):
    def __init__(self, _config_, converting_service):
        Thread.__init__(self)
        self.converting_service = converting_service
        self.subscriber = pubsub_v1.SubscriberClient()
        self.subscription_path = self.subscriber.subscription_path(_config_.get_pubsub_project(), _config_.get_pubsub_subscription())
        self.start()

        def callback(message):
            print('Received message: {}'.format(message))
            # Implementer un try except pour vérifier les donnees recues pour pas passer nptk à on message
            message.ack()
            msg_final = message.data
            self._on_message_(msg_final)

        self.subscriber.subscribe(self.subscription_path, callback=callback)

    def run(self):
         #while True:
         print('Listening for message...')
         time.sleep(60)

    def on_message(self, channel, method_frame, header_frame, body):
        logging.info(body)
        # logging.info('id = %s, URI = %s', body["id"], body['originPath'])
        # logging.info('URI = %s', body['originPath'])
        logging.info('URI = %s', body.decode())
        convert_request = json.loads(body.decode())
        logging.info(convert_request)
        self.converting_service.convert(convert_request["id"], convert_request['originPath'])

    def _on_message_(self,  body):
        logging.info(body)
        convert_request = json.loads(body.decode())
        self.converting_service.convert(convert_request["id"], convert_request['originPath'])